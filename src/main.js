import Vue from 'vue'
import App from './App.vue'
import store from './store'
import mDatePicker from 'vue-multi-date-picker'


Vue.config.productionTip = false
Vue.use(mDatePicker)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
