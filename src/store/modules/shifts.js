export default {
    // to beck end request   
    actions: {
    },

    //for work with sync function and change state
    mutations: {
      updateShifts(state, shift, index) {
        state.shifts[index] = shift
      },

      createShift(state, newShift) {
        state.shifts.unshift(newShift)
      },

      setIsEditPanelOpen(state,flag){
        state.isEditPanelOpen = flag
      },

      deleteShift(state, item){
        const index = state.shifts.indexOf(item);
        if (index !== -1) {
            state.shifts.splice(index, 1);
        }
      }

    },

    // my state
    state: {
      shifts: [],
      isEditPanelOpen:false
    },

    // get data from state
    getters: {
      getShift(state, index){
        return state.shifts[index]
      },
      allShifts(state) {
        return state.shifts
      },

      getIsEditPanelOpen(state){
          return state.isEditPanelOpen
      }
    }
  }
  